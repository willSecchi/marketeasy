import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class ProductsList extends StatefulWidget {
  final String token;
  ProductsList(this.token);
  @override
  _ProductsListState createState() => _ProductsListState(token);
}

class _ProductsListState extends State<ProductsList> {
  final String token;
  _ProductsListState(this.token);
  bool atualiza = false;


  Future<List<dynamic>> fetchUsers() async {
    var client = http.Client();
    var header = {"Content-Type": "application/json", "token": token, "Accept": "*/*"};
    var response = await client.get(Uri.parse('http://servicosflex.rpinfo.com.br:9000/v2.0/produtounidade/listaprodutos/0/unidade/83402711000110'), headers: header);
    return json.decode(response.body)['response']['produtos'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  _body() {
    return Container(
      child: FutureBuilder<List<dynamic>>(
        future: fetchUsers(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return RefreshIndicator(
              onRefresh: _onRefresh,
              child: ListView.builder(
                  padding: EdgeInsets.all(8),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {

                    return Card(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                           leading: Icon(Icons.info_outline),
                           title: Text("${snapshot.data[index]['Descricao']} - ${snapshot.data[index]['Codigo']}"),
                            subtitle: Text("Cód. Barras: ${snapshot.data[index]['CodigoBarras']}"),
                            trailing: Text("R\$: ${snapshot.data[index]['Preco']}"),
                            onTap: () {
                             print("Hey, você clicou em mim!");
                            },
                          )
                        ],
                      ),
                    );
                  }),
            );
          }
        },
      ),
    );
  }

  Future<void> _onRefresh() {
    return Future.delayed(Duration(seconds: 2), () {
      setState(() => atualiza = true);
    });
  }
}
