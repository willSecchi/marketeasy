import 'package:demoapp/login/login_screen.dart';
import 'package:demoapp/main/products_list.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  final String token;

  const MainScreen({Key key, this.token}) : super(key: key);
  @override
  _MainScreenState createState() => _MainScreenState(token);
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  int estado = 0;
  final String token;
  _MainScreenState(this.token);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: _appbar(),
      drawer: _drawer(),
      body: _body(),
      bottomNavigationBar: _bottinNavigation(),
    );
  }

  _appbar(){
    return AppBar(
      title: Text("Produtos"),
      leading: IconButton(
        icon: Icon(Icons.menu_rounded),
        onPressed: () => _scaffoldKey.currentState.openDrawer(),
      ),
      centerTitle: true,
      backgroundColor: Color(0xFF037cbc),
    );
  }

  _drawer(){
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("William Secchi"),
            accountEmail: Text("jeansecchii@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage(
                'assets/will.png'
              ),
            ),
            decoration: BoxDecoration(
              color: Color(0xFF037cbc)
            ),
          ),
          ListTile(
            leading: Icon(Icons.close),
            title: GestureDetector(
                child: Text("Sair"),
                onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()));
                setState(() {
                  estado = 0;
                });
                }
            ),

          )
        ],
      )
    );
  }

  _body(){
    if (estado == 0) {
      return Center(
        child: Text("Nenhum produto a ser exibido no momento!",
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        ),
      );
    } else {
      return ProductsList(token);
    }
}
  _bottinNavigation() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
      backgroundColor: Color(0xFF037cbc),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white.withOpacity(.6),
      selectedFontSize: 15,
      unselectedFontSize: 15,
      onTap: (value) {
        // Respond to item press.
        setState(() {
          _currentIndex = value;
          estado = 1;
        });
      },
      items: [
        BottomNavigationBarItem(
          // ignore: deprecated_member_use
          title: Text((estado == 0) ? 'Carregar produtos' : 'Produtos disponíveis'),
          icon: Icon(Icons.refresh),
        ),
      ],
    );
  }
}
