import 'dart:convert';
import 'package:demoapp/login/login_model.dart';
import 'package:http/http.dart' as http;

class LoginApi {
  static Future<Usuario> login(String user, String password) async {
    var response;

    var header = {"Content-Type": "application/json"};

    Map params = {"usuario": user, "senha": password};

    var _body = json.encode(params);

    var client = http.Client();
    try {
      response = await client.post(Uri.parse('http://servicosflex.rpinfo.com.br:9000/v1.1/auth'), headers: header, body: _body);
    } finally {
      client.close();
    }

    if(response.statusCode == 200){
      Map mapResponse = jsonDecode(response.body);

      final usuario = Usuario.fromJson(mapResponse);

      return usuario;
    } else {
      throw Exception('Falha ao acessar api');
    }
  }
}
