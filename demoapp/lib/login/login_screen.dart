import 'package:demoapp/login/login_api.dart';
import 'package:demoapp/main/main_screen.dart';
import 'package:demoapp/utils/nav.dart';
import 'package:demoapp/widgets/alert.dart';
import 'package:demoapp/widgets/white_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _ctrlLogin = TextEditingController();
  final _ctrlSenha = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: buildContainer(context),
    ));
  }

  Container buildContainer(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/wallpaper1.png"), fit: BoxFit.cover)),
      child: ListView(
        padding: EdgeInsets.only(left: 25.0, right: 25.0),
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: 180, left: 40, right: 40, bottom: 40),
                  ),
                  _inputUser(),
                  SizedBox(
                    height: 10.0,
                  ),
                  _inputPassword(),
                  _fgPassword(),
                  SizedBox(
                    height: 20,
                  ),
                  _entrar(context), // SignUpButton          SignUpButton(),
                  SizedBox(
                    height: 20,
                  ),
                  _cadastro()
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  TextFormField _inputUser() {
    return TextFormField(
      style: TextStyle(color: Colors.white),
      controller: _ctrlLogin,
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white, fontFamily: "WorkSansLight", fontSize: 17.0),
        hintText: "Usuário",
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        prefixIcon: const Icon(
          Icons.person,
          color: Colors.white,
        ),
      ),
      inputFormatters: [
        // ignore: deprecated_member_use
        FilteringTextInputFormatter.digitsOnly
      ],
      keyboardType: TextInputType.number,
      cursorColor: Colors.white,
      validator: (text) {
        if (text.isEmpty) return "Insira um usuário!";
        return null;
      },
    );
  }

  TextFormField _inputPassword() {
    return TextFormField(
      style: TextStyle(color: Colors.white),
      controller: _ctrlSenha,
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white, fontFamily: "WorkSansLight", fontSize: 17.0),
        hintText: "Senha",
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        prefixIcon: const Icon(
          Icons.lock_outline,
          color: Colors.white,
        ),
      ),
      obscureText: true,
      cursorColor: Colors.white,
      validator: (text) {
        if (text.isEmpty) return "Insira uma senha!";
        return null;
      },
    );
  }

  SizedBox _cadastro() {
    return SizedBox(
      height: 50,
      width: 300,
      child: WhiteButton(
        "Cadastro",
        onPressed: () => print("fui clicado!"),
      ),
    );
  }

  SizedBox _entrar(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 300,
      child: WhiteButton("Entrar", onPressed: () {
        _onClickButton(context);
      }),
    );
  }

  Align _fgPassword() {
    return Align(
      alignment: Alignment.centerRight,
      // ignore: deprecated_member_use
      child: FlatButton(
        onPressed: () {
          print("Ei, você tá me analisando! ;D ");
        },
        child: Text(
          "Esqueci minha senha",
          textAlign: TextAlign.right,
          style: TextStyle(color: Colors.white, fontSize: 14.0),
        ),
        padding: EdgeInsets.zero,
      ),
    );
  }

  Future<void> _onClickButton(context) async {
    bool formOk = _formKey.currentState.validate();

    if (!formOk) {
      return;
    }

    String login = _ctrlLogin.text;
    String senha = _ctrlSenha.text;

    var usuario = await LoginApi.login(login, senha);

    if (usuario.response.status == 'error') {
      alert(context, "Usuário ou senha incorretos.");
    } else {
      if (usuario.response.token != null && usuario.response.token.isNotEmpty) {
        push(
            context,
            MainScreen(
              token: usuario.response.token,
            ));
      } else {
        throw Exception("Erro!");
      }
    }
  }
}
