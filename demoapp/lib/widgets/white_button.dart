import 'package:flutter/material.dart';

class WhiteButton extends StatelessWidget {
  String text;
  Function onPressed;

  WhiteButton(this.text, {this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color: Colors.white,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 18.0,
          ),
        ),
        onPressed: onPressed,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)));
  }
}
